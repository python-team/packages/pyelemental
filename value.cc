/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "value.hh"

namespace pyElemental {



bool
init_value (PyObject* module)
{
#define ADD_INT_CONSTANT(name) \
PyModule_AddIntConstant (module, #name, Elemental::name)

	PyObject *std_temp = PyFloat_FromDouble (Elemental::STANDARD_TEMPERATURE);

	return
		std_temp != NULL &&
		PyModule_AddObject (module, "STANDARD_TEMPERATURE", std_temp) == 0 &&

		ADD_INT_CONSTANT (Q_NEUTRAL) == 0 &&
		ADD_INT_CONSTANT (Q_UNK) == 0 &&
		ADD_INT_CONSTANT (Q_NA) == 0 &&
		ADD_INT_CONSTANT (Q_EST) == 0 &&
		ADD_INT_CONSTANT (Q_CA) == 0 &&
		ADD_INT_CONSTANT (Q_ISO) == 0 &&

		color::ready (module) &&
		EntriesView::ready (module) &&
		EntriesStream::ready (module) &&
		value_base::ready (module) &&
		color_value_base::ready (module);
}


//******************************************************************************
// class color


bool
color::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "color", X_PYOBJECT (&type)) == 0;
}


PyObject*
color::wrap (const cxxtype& source)
{
	return wrap_copy (&type, source);
}


PyTypeObject
color::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.color",
	sizeof (pytype), 0,
	destructor (dealloc), 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, reprfunc (get_hex_spec), 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"A color description.",
	0, 0, 0, 0, 0, 0,
	methods, 0, get_set,
	0, 0, 0, 0, 0, initproc (init), 0, newfunc (create),
	0, 0, 0, 0, 0, 0, 0, 0
};


PyGetSetDef
color::get_set[] =
{
	{ "red", getter (get_red), setter (set_red),
		"The red component of the color in the RGB space.", NULL },
	{ "green", getter (get_green), setter (set_green),
		"The green component of the color in the RGB space.", NULL },
	{ "blue", getter (get_blue), setter (set_blue),
		"The blue component of the color in the RGB space.", NULL },
	{ "luminance", getter (get_luminance), 0,
		"The luminous intensity of the color.", NULL },
	{ "compliment", getter (get_compliment), 0,
		"A grayscale color complimentary in luminance to this color.", NULL },
	{ "hex_spec", getter (get_hex_spec), 0,
		"A hexadecimal specification of the color (\"#rrggbb\" format).", NULL},
	{ NULL, NULL, NULL, NULL, NULL }
};


PyMethodDef
color::methods[] =
{
	{ "composite", PyCFunction (composite), METH_VARARGS,
		"(other, alpha) Returns a composite of this color and another." },
	{ NULL, NULL, 0, NULL }
};


int
color::init (pytype* self, PyObject* args, PyObject* kwargs)
{
	double red = 0.0, green = 0.0, blue = 0.0;
	static char *kwlist[] = { "red", "green", "blue", NULL };

	if (!PyArg_ParseTupleAndKeywords (args, kwargs, "|ddd", kwlist,
		&red, &green, &blue))
		return -1;
	
	delete self->cxxobj;
	self->cxxobj = new cxxtype (red, green, blue);
	self->owned = true;
	return 0;
}


PyObject*
color::get_red (pytype* self, void*)
{
	return PyFloat_FromDouble (self->cxxobj->red);
}


int
color::set_red (pytype* self, PyObject* value, void*)
{
	if (!X_PyObject_CheckAttr (value, &PyFloat_Type, "component", &type))
		return -1;

	self->cxxobj->red = PyFloat_AsDouble (value);
	return 0;
}


PyObject*
color::get_green (pytype* self, void*)
{
	return PyFloat_FromDouble (self->cxxobj->green);
}


int
color::set_green (pytype* self, PyObject* value, void*)
{
	if (!X_PyObject_CheckAttr (value, &PyFloat_Type, "component", &type))
		return -1;

	self->cxxobj->green = PyFloat_AsDouble (value);
	return 0;
}


PyObject*
color::get_blue (pytype* self, void*)
{
	return PyFloat_FromDouble (self->cxxobj->blue);
}


int
color::set_blue (pytype* self, PyObject* value, void*)
{
	if (!X_PyObject_CheckAttr (value, &PyFloat_Type, "component", &type))
		return -1;

	self->cxxobj->blue = PyFloat_AsDouble (value);
	return 0;
}


PyObject*
color::get_luminance (pytype* self, void*)
{
	return PyFloat_FromDouble (self->cxxobj->get_luminance ());
}


PyObject*
color::get_compliment (pytype* self, void*)
{
	return wrap (self->cxxobj->get_compliment ());
}


PyObject*
color::get_hex_spec (pytype* self, void*)
{
	return X_PyString_FromCxxString (self->cxxobj->get_hex_spec ());
}


PyObject*
color::composite (pytype* self, PyObject* args)
{
	PyObject *other = NULL;
	double alpha;
	
	if (!PyArg_ParseTuple (args, "O!d", &type, &other, &alpha))
		return NULL;

	return wrap (self->cxxobj->composite (unwrap (other), alpha));
}


//******************************************************************************
// class EntriesView::Unwrapper


class EntriesView::Unwrapper
:	public Elemental::EntriesView
{
public:

	Unwrapper (PyObject* pyobj);
	virtual ~Unwrapper ();

	virtual void header (const ustring& category) throw ();
	virtual void entry (const ustring& name, const ustring& value,
		const ustring& tip = ustring ()) throw ();

private:

	PyObject *pyobj;
};


EntriesView::Unwrapper::Unwrapper (PyObject* pyobj_)
:	pyobj (pyobj_)
{}


EntriesView::Unwrapper::~Unwrapper ()
{}


void
EntriesView::Unwrapper::header (const ustring& category) throw ()
{
	PyObject *result = PyObject_CallMethod (pyobj, "header", "N",
		X_PyUnicode_FromUstring (category));
	Py_XDECREF (result);
}


void
EntriesView::Unwrapper::entry (const ustring& name, const ustring& value,
	const ustring& tip) throw ()
{
	PyObject *result = PyObject_CallMethod (pyobj, "entry", "NNN",
		X_PyUnicode_FromUstring (name),
		X_PyUnicode_FromUstring (value),
		X_PyUnicode_FromUstring (tip));
	Py_XDECREF (result);
}


//******************************************************************************
// class EntriesView


bool
EntriesView::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "EntriesView", X_PYOBJECT (&type)) == 0;
}


PyObject*
EntriesView::wrap (const cxxtype& source)
{
	return wrap_ref (&type, source);
}


PyTypeObject
EntriesView::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.EntriesView",
	sizeof (pytype), 0,
	destructor (dealloc), 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"An interface for the display of value entries.",
	0, 0, 0, 0, 0, 0,
	methods, 0, 0,
	0, 0, 0, 0, 0, 0, 0, newfunc (create),
	0, 0, 0, 0, 0, 0, 0, 0
};


PyMethodDef
EntriesView::methods[] =
{
	{ "header", PyCFunction (header), METH_VARARGS,
		"(category) Displays a category header." },
	{ "entry", PyCFunction (entry), METH_VARARGS,
		"(name, value[, tip]) Displays a single value entry." },
	{ NULL, NULL, 0, NULL }
};


PyObject*
EntriesView::create (PyTypeObject* type, PyObject*, PyObject*)
{
	if (type == &EntriesView::type)
	{
		PyErr_Format (PyExc_TypeError, "cannot create '%.100s' instances",
			type->tp_name);
		return NULL;
	}

	pytype *self = reinterpret_cast<pytype*> (type->tp_alloc (type, 0));
	if (self != NULL)
	{
		self->cxxobj = new Unwrapper (X_PYOBJECT (self));
		self->owned = true;
	}
	return X_PYOBJECT (self);
}


PyObject*
EntriesView::header (pytype* self, PyObject* args)
{
	if (dynamic_cast<Unwrapper*> (self->cxxobj))
	{
		PyErr_SetString (PyExc_NotImplementedError, "pure virtual function");
		return NULL;
	}

	PyObject *category = NULL;
	if (!PyArg_ParseTuple (args, "U", &category))
		return NULL;

	self->cxxobj->header (X_PyUnicode_AsUstring (category));
	Py_RETURN_NONE;
}


PyObject*
EntriesView::entry (pytype* self, PyObject* args)
{
	if (dynamic_cast<Unwrapper*> (self->cxxobj))
	{
		PyErr_SetString (PyExc_NotImplementedError, "pure virtual function");
		return NULL;
	}

	PyObject *name = NULL, *value = NULL, *tip = NULL;
	if (!PyArg_ParseTuple (args, "UU|U", &name, &value, &tip))
		return NULL;

	self->cxxobj->entry (X_PyUnicode_AsUstring (name),
		X_PyUnicode_AsUstring (value), X_PyUnicode_AsUstring (tip));
	Py_RETURN_NONE;
}


//******************************************************************************
// class EntriesStream


bool
EntriesStream::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "EntriesStream", X_PYOBJECT (&type)) == 0;
}


PyObject*
EntriesStream::wrap (const cxxtype& source)
{
	return wrap_ref (&type, source);
}


PyTypeObject
EntriesStream::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.EntriesStream",
	sizeof (pytype), 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"An adapter for the display of value entries on an output stream.",
	0, 0, 0, 0, 0, 0,
	0, 0, 0,
	&EntriesView::type, 0, 0, 0, 0, initproc (init), 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};


int
EntriesStream::init (pytype* self, PyObject* args, PyObject* kwargs)
{
	PyObject *file = NULL;
	static char *kwlist[] = { "file", NULL };

	if (!PyArg_ParseTupleAndKeywords (args, kwargs, "O!", kwlist,
		&PyFile_Type, &file))
		return -1;
	
	delete self->cxxobj;
	self->cxxobj = new cxxtype (PyFile_AsFile (file));
	self->owned = true;
	return 0;
}


//******************************************************************************
// class value_base


bool
value_base::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "value_base", X_PYOBJECT (&type)) == 0;
}


PyTypeObject
value_base::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.value_base",
	sizeof (pytype), 0,
	destructor (dealloc), 0, 0, 0, cmpfunc (compare), 0,
	0, 0, 0,
	0, 0, reprfunc (str), 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"A base class for qualified values.",
	0, 0, 0, 0, 0, 0,
	methods, 0, get_set,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};


PyGetSetDef
value_base::get_set[] =
{
	{ "has_value", getter (get_has_value), NULL,
		"Whether the value is defined.", NULL },
	{ "qualifier", getter (get_qualifier), setter (set_qualifier),
		"The qualification of the value's meaning.", NULL },
	{ "tip", getter (get_tip), NULL,
		"A localized message to be displayed as a tooltip.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }
};


PyMethodDef
value_base::methods[] =
{
	{ "get_string", PyCFunction (get_string), METH_VARARGS,
		"([format]) Returns a localized text representation of the value." },
	{ "make_entry", PyCFunction (make_entry), METH_VARARGS,
		"(view, name[, format]) Possibly adds the result of get_string() to an "
		"EntriesView." },
	{ NULL, NULL, 0, NULL }
};


int
value_base::compare (pytype* self, pytype* other)
{
	return self->cxxobj->compare (*other->cxxobj);
}


PyObject*
value_base::str (pytype* self)
{
	return X_PyUnicode_FromUstring (self->cxxobj->get_string ());
}


PyObject*
value_base::get_has_value (pytype* self, void*)
{
	return PyBool_FromLong (self->cxxobj->has_value ());
}


PyObject*
value_base::get_qualifier (pytype* self, void*)
{
	return PyInt_FromLong (self->cxxobj->qualifier);
}


int
value_base::set_qualifier (PyObject* self_, PyObject* value_, void*)
{
	if (!X_PyObject_CheckAttr (value_, &PyInt_Type, "qualifier", &type))
		return -1;

	pytype *self = reinterpret_cast<pytype*> (self_);
	long value = PyInt_AsLong (value_);	

	switch (value)
	{
	case Elemental::Q_NEUTRAL:
	case Elemental::Q_UNK:
	case Elemental::Q_NA:
	case Elemental::Q_EST:
	case Elemental::Q_CA:
	case Elemental::Q_ISO:
		self->cxxobj->qualifier = Elemental::Qualifier (value);
		return 0;
	default:
		PyErr_SetString (PyExc_ValueError,
			"The given int is not a valid Elemental.value_base qualifier.");
		return -1;
	}
}


PyObject*
value_base::get_tip (pytype* self, void*)
{
	return X_PyUnicode_FromUstring (self->cxxobj->get_tip ());
}


PyObject*
value_base::get_string (pytype* self, PyObject* args)
{
	PyObject *format = NULL;
	if (!PyArg_ParseTuple (args, "|U", &format))
		return NULL;

	return X_PyUnicode_FromUstring
		(self->cxxobj->get_string (X_PyUnicode_AsUstring (format)));
}


PyObject*
value_base::make_entry (pytype* self, PyObject* args)
{
	PyObject *view = NULL, *name = NULL, *format = NULL;

	if (!PyArg_ParseTuple (args, "O!U|U",
		&EntriesView::type, &view, &name, &format))
		return NULL;

	self->cxxobj->make_entry (EntriesView::unwrap (view),
		X_PyUnicode_AsUstring (name), X_PyUnicode_AsUstring (format));
	Py_RETURN_NONE;
}


//******************************************************************************
// class color_value_base


bool
color_value_base::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "color_value_base",
			X_PYOBJECT (&type)) == 0;
}


PyTypeObject
color_value_base::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.color_value_base",
	sizeof (pytype), 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"A base class for qualified values which have representative colors.",
	0, 0, 0, 0, 0, 0,
	0, 0, get_set,
	&value_base::type, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};


PyGetSetDef
color_value_base::get_set[] =
{
	{ "color", getter (get_color), NULL,
		"A color representing the value.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }
};


PyObject*
color_value_base::get_color (pytype* self, void*)
{
	return color::wrap (self->cxxobj->get_color ());
}


} // namespace pyElemental
