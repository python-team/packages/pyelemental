/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PYELEMENTAL__ELEMENT_HH
#define PYELEMENTAL__ELEMENT_HH

#include "value-types.hh"
#include <libelemental/element.hh>

namespace pyElemental {

bool init_element (PyObject* module);

//******************************************************************************

class Property
:	public CxxWrapper<Elemental::PropertyBase>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	static PyMethodDef methods[];

	static PyObject* get_name (pytype* self, void*);
	static PyObject* has_format (pytype* self, void*);
	static PyObject* get_format (pytype* self, void*);
	static PyObject* get_description (pytype* self, void*);
	static PyObject* get_sources (pytype* self, void*);
	static PyObject* is_colorable (pytype* self, void*);

	static PyObject* make_entry (pytype* self, PyObject* args);
};

//******************************************************************************

class FloatProperty
:	public CxxWrapper<Elemental::FloatProperty>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	static PyMethodDef methods[];

	static PyObject* is_scale_valid (pytype* self, void*);
	static PyObject* get_minimum (pytype* self, void*);
	static PyObject* get_medium (pytype* self, bool logarithmic);
	static PyObject* get_maximum (pytype* self, void*);

	static PyObject* get_scale_position (pytype* self, PyObject* args);
};

//******************************************************************************

class Category
:	public CxxWrapper<Elemental::Category>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	static PyMethodDef methods[];

	static PyObject* get_name (pytype* self, void*);
	static PyObject* get_properties (pytype* self, void*);

	static PyObject* make_header (pytype* self, PyObject* args);
};

//******************************************************************************

class Element
:	public CxxWrapper<Elemental::Element>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	static PyMethodDef methods[];

	template<typename proptype> static PyObject* get_property (PyObject* self,
		void* prop);

	static PyObject* get_symbol (pytype* self, void*);
	static PyObject* get_number (pytype* self, void*);

	static PyObject* get_phase (pytype* self, PyObject* args);
	static PyObject* make_entries (pytype* self, PyObject* args,
		PyObject* kwargs);
};

} // namespace pyElemental

#endif // PYELEMENTAL__ELEMENT_HH
