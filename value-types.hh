/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PYELEMENTAL__VALUE_TYPES_HH
#define PYELEMENTAL__VALUE_TYPES_HH

#include "value.hh"
#include <libelemental/value-types.hh>

namespace pyElemental {

bool init_value_types (PyObject* module);

//******************************************************************************

template<typename get_type, typename set_type>
struct ValueTypeInfo
{
	const char *name, *longname;
	const char *description;
	PyTypeObject *pyparent;

	PyTypeObject *pyvaltype;
	PyObject* (*get_transform) (get_type source);
	set_type (*set_transform) (PyObject* value);
};

#define ValueType_template template<typename cxxtype_, typename get_type,\
typename set_type, const ValueTypeInfo<get_type, set_type> &info>

#define ValueType_generic ValueType<cxxtype_, get_type, set_type, info>

#define ValueType_t(middle) ValueType_template middle ValueType_generic

ValueType_template
class ValueType
:	public CxxWrapper<cxxtype_>
{
public:

	typedef CxxWrapper<cxxtype_> wrapper;
	typedef typename wrapper::cxxtype cxxtype;
	typedef typename wrapper::pytype pytype;

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	
	static int init (pytype* self, PyObject* args, PyObject* kwargs);

	static PyObject* get_value (pytype* self, void*);
	static int set_value (pytype* self, PyObject* value, void*);
};

#define DECLARE_VALUE_TYPE(name, get_type, set_type)\
extern const ValueTypeInfo<get_type, set_type> name##_info;\
typedef ValueType<Elemental::name, get_type, set_type, name##_info> name;

//******************************************************************************

template<typename cxxtype_,
	const ValueTypeInfo<long, typename cxxtype_::Value> &info>
class EnumValueType
:	public ValueType<cxxtype_, long, typename cxxtype_::Value, info>
{
public:

	typedef ValueType<cxxtype_, long, typename cxxtype_::Value, info> parent;

	static bool ready (PyObject* module);

protected:

	static bool add_value (char* name, typename cxxtype_::Value value);
};

#define DECLARE_ENUM_VALUE_TYPE(name)\
extern const ValueTypeInfo<long, Elemental::name::Value> name##_info;\
typedef EnumValueType<Elemental::name, name##_info> name;\
template<> bool EnumValueType<Elemental::name, name##_info>::ready\
	(PyObject* module);

#define ADD_ENUM_VALUE(name) add_value (#name, cxxtype::name)

//******************************************************************************

#define ValueListType_generic ValueListType<cxxtype_, get_type, set_type, info>

#define ValueListType_t(middle) ValueType_template middle ValueListType_generic

ValueType_template
class ValueListType
:	public CxxWrapper<cxxtype_>
{
public:

	typedef CxxWrapper<cxxtype_> wrapper;
	typedef typename wrapper::cxxtype cxxtype;
	typedef typename wrapper::pytype pytype;

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	
	static int init (pytype* self, PyObject* args, PyObject* kwargs);

	static PyObject* get_values (pytype* self, void*);
	static int set_values (pytype* self, PyObject* values, void*);
};

#define DECLARE_VALUE_LIST_TYPE(name, get_type, set_type)\
extern const ValueTypeInfo<get_type, set_type> name##_info;\
typedef ValueListType<Elemental::name, get_type, set_type, name##_info> name;

//******************************************************************************

DECLARE_VALUE_TYPE (Float, double, double)
DECLARE_VALUE_TYPE (Int, long, long)
DECLARE_VALUE_TYPE (String, const ustring&, ustring)

DECLARE_VALUE_LIST_TYPE (FloatList, double, double)
DECLARE_VALUE_LIST_TYPE (IntList, long, long)
	
DECLARE_VALUE_TYPE (Message, const ustring&, ustring)

//******************************************************************************

class Event
:	public CxxWrapper<Elemental::Event>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	
	static int init (pytype* self, PyObject* args, PyObject* kwargs);

	static PyObject* get_when (pytype* self, void*);
	static int set_when (pytype* self, PyObject* value, void*);

	static PyObject* get_where (pytype* self, void*);
	static int set_where (pytype* self, PyObject* value, void*);
};
	
//******************************************************************************

DECLARE_ENUM_VALUE_TYPE (Series)
DECLARE_ENUM_VALUE_TYPE (Block)
DECLARE_ENUM_VALUE_TYPE (Phase)
DECLARE_ENUM_VALUE_TYPE (LatticeType)

DECLARE_VALUE_TYPE (ColorValue, const Elemental::color&, Elemental::color&)

} // namespace pyElemental

#include "value-types.tcc"

#endif // PYELEMENTAL__VALUE_TYPES_HH
