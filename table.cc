/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "table.hh"

#include <clocale>
#include <locale>


PyMODINIT_FUNC
initElemental (void)
{
	try
		{ std::locale::global (std::locale (std::setlocale (LC_ALL, ""))); }
	catch (std::runtime_error)
		{ std::locale::global (std::locale::classic ()); }

	Elemental::initialize ();
	pyElemental::the_module::ready ();
}


//******************************************************************************

namespace pyElemental {


//******************************************************************************
// module Elemental


bool
the_module::ready ()
{
	PyObject *module = Py_InitModule3 ("Elemental", methods,
		"A periodic table module with detailed information on elements.");
	if (module == NULL) return false;
	Py_INCREF (module);

	PyObject *table = NULL;

	bool ready =
		init_value (module) &&
		init_value_types (module) &&
		init_element (module) &&
		(table = wrap_table ()) &&
		PyModule_AddObject (module, "table", table);

	Py_DECREF (module);
	return ready;
}


PyObject*
the_module::wrap_table ()
{
	const Elemental::Table &table = Elemental::get_table ();

	PyObject *result = PyList_New (table.size ());
	if (result == NULL) return NULL;

	int n = 0;
	for (Elemental::Table::const_iterator i = table.begin (); i != table.end ();
		++i)
	{
		PyObject *element = Element::wrap (**i);
		if (element != NULL)
			PyList_SetItem (result, n++, element);
	}

	return result;
}


PyMethodDef
the_module::methods[] =
{
	{ "get_element", get_element, METH_VARARGS,
		"(which) Returns the element with a given symbol or atomic number." },
	{ NULL, NULL, 0, NULL }
};


PyObject*
the_module::get_element (PyObject*, PyObject* args)
{
	PyObject *which;
	const Element::cxxtype *element = NULL;
	
	if (!PyArg_ParseTuple (args, "O", &which)) return NULL;
	
	if (PyInt_Check (which))
	{
		try
			{ element = &Elemental::get_element (PyInt_AsLong (which)); }
		catch (std::out_of_range& e)
		{
			PyErr_SetString (PyExc_KeyError, e.what ());
			return NULL;
		}
	}
	else if (PyString_Check (which))
	{
		try
			{ element = &Elemental::get_element (PyString_AsString (which)); }
		catch (std::invalid_argument& e)
		{
			PyErr_SetString (PyExc_KeyError, e.what ());
			return NULL;
		}
	}
	else
	{
		PyErr_SetString (PyExc_TypeError, "argument 1 must be int or str");
		return NULL;
	}

	PyObject *table = PyObject_GetAttrString
		(PyImport_AddModule ("Elemental"), "table");
	return PySequence_GetItem (table, element->number - 1);
}


} // namespace pyElemental
