/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "value-types.hh"

#include <stdexcept>

namespace pyElemental {


bool
init_value_types (PyObject* module)
{
	return
		Float::ready (module) &&
		Int::ready (module) &&
		String::ready (module) &&

		FloatList::ready (module) &&
		IntList::ready (module) &&

		Message::ready (module) &&
		Event::ready (module) &&

		Series::ready (module) &&
		Block::ready (module) &&
		Phase::ready (module) &&
		LatticeType::ready (module) &&

		ColorValue::ready (module);
}


//******************************************************************************
// class Float


const ValueTypeInfo<double, double>
Float_info =
{
	"Float", "Elemental.Float",
	"A qualified floating-point value.",
	&value_base::type,
	&PyFloat_Type,
	PyFloat_FromDouble,
	PyFloat_AsDouble
};


//******************************************************************************
// class Int


const ValueTypeInfo<long, long>
Int_info =
{
	"Int", "Elemental.Int",
	"A qualified integer value.",
	&value_base::type,
	&PyInt_Type,
	PyInt_FromLong,
	PyInt_AsLong
};


//******************************************************************************
// class String


const ValueTypeInfo<const ustring&, ustring>
String_info =
{
	"String", "Elemental.String",
	"A qualified UTF-8 string value that does not require translation.",
	&value_base::type,
	&PyUnicode_Type,
	X_PyUnicode_FromUstring,
	X_PyUnicode_AsUstring
};


//******************************************************************************
// class FloatList


const ValueTypeInfo<double, double>
FloatList_info =
{
	"FloatList", "Elemental.FloatList",
	"A qualified list of floating-point values.",
	&value_base::type,
	&PyFloat_Type,
	PyFloat_FromDouble,
	PyFloat_AsDouble
};


//******************************************************************************
// class IntList


const ValueTypeInfo<long, long>
IntList_info =
{
	"IntList", "Elemental.IntList",
	"A qualified list of integer values.",
	&value_base::type,
	&PyInt_Type,
	PyInt_FromLong,
	PyInt_AsLong
};


//******************************************************************************
// class Message


const ValueTypeInfo<const ustring&, ustring>
Message_info =
{
	"Message", "Elemental.Message",
	"A qualified UTF-8 string value that requires translation.",
	&value_base::type,
	&PyUnicode_Type,
	X_PyUnicode_FromUstring,
	X_PyUnicode_AsUstring
};


//******************************************************************************
// class Event


bool
Event::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "Event", X_PYOBJECT (&type)) == 0;
}


PyObject*
Event::wrap (const cxxtype& source)
{
	return wrap_copy (&type, source);
}


PyTypeObject
Event::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.Event",
	sizeof (pytype), 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT,
	"A qualified value representing the time and location of an event.",
	0, 0, 0, 0, 0, 0,
	0, 0, get_set,
	&value_base::type, 0, 0, 0, 0, initproc (init), 0, newfunc (create),
	0, 0, 0, 0, 0, 0, 0, 0
};


PyGetSetDef
Event::get_set[] =
{
	{ "when", getter (get_when), setter (set_when),
		"The year in which the event occurred, if defined.", NULL },
	{ "where", getter (get_where), setter (set_where),
		"The place in which the event occurred, if defined.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }\
};


int
Event::init (pytype* self, PyObject* args, PyObject* kwargs)
{
	PyObject *when = NULL, *where = NULL, *qual = NULL;
	static char *kwlist[] = { "when", "where", "qualifier", NULL };

	if (!PyArg_ParseTupleAndKeywords (args, kwargs, "|OOO", kwlist,
		&when, &where, &qual))
		return -1;

	if (when != NULL && where != NULL)
	{
		if (set_when (self, when, NULL) < 0) return -1;
		if (set_where (self, where, NULL) < 0) return -1;
		if (qual == NULL)
			self->cxxobj->qualifier = Elemental::Q_NEUTRAL;
	}
	else if (when != NULL || where != NULL)
	{
		PyErr_SetString (PyExc_TypeError,
			"when and where must both be specified");
		return -1;
	}

	if (qual != NULL)
		if (value_base::set_qualifier (X_PYOBJECT (self), qual, NULL) < 0)
			return -1;

	return 0;
}


PyObject*
Event::get_when (pytype* self, void*)
{
	return PyInt_FromLong (self->cxxobj->when);
}


int
Event::set_when (pytype* self, PyObject* value, void*)
{
	if (!X_PyObject_CheckAttr (value, &PyInt_Type, "time", &type))
		return -1;

	self->cxxobj->when = PyInt_AsLong (value);
	return 0;
}


PyObject*
Event::get_where (pytype* self, void*)
{
	return X_PyUnicode_FromUstring (self->cxxobj->where);
}


int
Event::set_where (pytype* self, PyObject* value, void*)
{
	if (!X_PyObject_CheckAttr (value, &PyUnicode_Type, "location", &type))
		return -1;

	self->cxxobj->where = X_PyUnicode_AsUstring (value);
	return 0;
}


//******************************************************************************
// class Series


template<>
bool
EnumValueType<Elemental::Series, Series_info>::ready (PyObject* module)
{
	return parent::ready (module) &&
		ADD_ENUM_VALUE (NONMETAL) &&
		ADD_ENUM_VALUE (NOBLE_GAS) &&
		ADD_ENUM_VALUE (ALKALI_METAL) &&
		ADD_ENUM_VALUE (ALKALINE_EARTH_METAL) &&
		ADD_ENUM_VALUE (SEMIMETAL) &&
		ADD_ENUM_VALUE (HALOGEN) &&
		ADD_ENUM_VALUE (POST_TRANSITION_METAL) &&
		ADD_ENUM_VALUE (TRANSITION_METAL) &&
		ADD_ENUM_VALUE (LANTHANIDE) &&
		ADD_ENUM_VALUE (ACTINIDE);
};


static Elemental::Series::Value
Series_set_transform (PyObject* value_)
{
	long value = PyInt_AsLong (value_);
	switch (value)
	{
	case Elemental::Series::NONMETAL:
	case Elemental::Series::NOBLE_GAS:
	case Elemental::Series::ALKALI_METAL:
	case Elemental::Series::ALKALINE_EARTH_METAL:
	case Elemental::Series::SEMIMETAL:
	case Elemental::Series::HALOGEN:
	case Elemental::Series::POST_TRANSITION_METAL:
	case Elemental::Series::TRANSITION_METAL:
	case Elemental::Series::LANTHANIDE:
	case Elemental::Series::ACTINIDE:
		return Elemental::Series::Value (value);
	default:
		throw std::invalid_argument ("Series");
	}
}


const ValueTypeInfo<long, Elemental::Series::Value>
Series_info =
{
	"Series", "Elemental.Series",
	"A qualified value representing a chemical series.",
	&color_value_base::type,
	&PyInt_Type,
	PyInt_FromLong,
	Series_set_transform
};


//******************************************************************************
// class Block


template<>
bool
EnumValueType<Elemental::Block, Block_info>::ready (PyObject* module)
{
	return parent::ready (module) &&
		ADD_ENUM_VALUE (S) &&
		ADD_ENUM_VALUE (P) &&
		ADD_ENUM_VALUE (D) &&
		ADD_ENUM_VALUE (F);
};


static Elemental::Block::Value
Block_set_transform (PyObject* value_)
{
	long value = PyInt_AsLong (value_);
	switch (value)
	{
	case Elemental::Block::S:
	case Elemental::Block::P:
	case Elemental::Block::D:
	case Elemental::Block::F:
		return Elemental::Block::Value (value);
	default:
		throw std::invalid_argument ("Block");
	}
}


const ValueTypeInfo<long, Elemental::Block::Value>
Block_info =
{
	"Block", "Elemental.Block",
	"A qualified value representing a periodic table block.",
	&color_value_base::type,
	&PyInt_Type,
	PyInt_FromLong,
	Block_set_transform
};


//******************************************************************************
// class Phase


template<>
bool
EnumValueType<Elemental::Phase, Phase_info>::ready (PyObject* module)
{
	return parent::ready (module) &&
		ADD_ENUM_VALUE (SOLID) &&
		ADD_ENUM_VALUE (LIQUID) &&
		ADD_ENUM_VALUE (GAS);
};


static Elemental::Phase::Value
Phase_set_transform (PyObject* value_)
{
	long value = PyInt_AsLong (value_);
	switch (value)
	{
	case Elemental::Phase::SOLID:
	case Elemental::Phase::LIQUID:
	case Elemental::Phase::GAS:
		return Elemental::Phase::Value (value);
	default:
		throw std::invalid_argument ("Phase");
	}
}


const ValueTypeInfo<long, Elemental::Phase::Value>
Phase_info =
{
	"Phase", "Elemental.Phase",
	"A qualified value representing a phase of matter.",
	&color_value_base::type,
	&PyInt_Type,
	PyInt_FromLong,
	Phase_set_transform
};


//******************************************************************************
// class LatticeType


template<>
bool
EnumValueType<Elemental::LatticeType, LatticeType_info>::ready
	(PyObject* module)
{
	return parent::ready (module) &&
		ADD_ENUM_VALUE (TRI) &&
		ADD_ENUM_VALUE (MONO) &&
		ADD_ENUM_VALUE (ORTH) &&
		ADD_ENUM_VALUE (TET) &&
		ADD_ENUM_VALUE (RHO) &&
		ADD_ENUM_VALUE (HEX) &&
		ADD_ENUM_VALUE (SC) &&
		ADD_ENUM_VALUE (BCC) &&
		ADD_ENUM_VALUE (FCC);
};


static Elemental::LatticeType::Value
LatticeType_set_transform (PyObject* value_)
{
	long value = PyInt_AsLong (value_);
	switch (value)
	{
	case Elemental::LatticeType::TRI:
	case Elemental::LatticeType::MONO:
	case Elemental::LatticeType::ORTH:
	case Elemental::LatticeType::TET:
	case Elemental::LatticeType::RHO:
	case Elemental::LatticeType::HEX:
	case Elemental::LatticeType::SC:
	case Elemental::LatticeType::BCC:
	case Elemental::LatticeType::FCC:
		return Elemental::LatticeType::Value (value);
	default:
		throw std::invalid_argument ("LatticeType");
	}
}


const ValueTypeInfo<long, Elemental::LatticeType::Value>
LatticeType_info =
{
	"LatticeType", "Elemental.LatticeType",
	"A qualified value representing a Bravais lattice type.",
	&color_value_base::type,
	&PyInt_Type,
	PyInt_FromLong,
	LatticeType_set_transform
};


//******************************************************************************
// class ColorValue


const ValueTypeInfo<const Elemental::color&, Elemental::color&>
ColorValue_info =
{
	"ColorValue", "Elemental.ColorValue",
	"A qualified value representing a display color.",
	&color_value_base::type,
	&color::type,
	color::wrap,
	color::unwrap
};


} // namespace pyElemental
