/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PYELEMENTAL__MISC_HH
#define PYELEMENTAL__MISC_HH

#include <Python.h>
#include <string>
#include <glibmm/ustring.h>

#define X_PYOBJECT(obj) reinterpret_cast<PyObject*> (obj)

bool X_PyType_AddIntConstant (PyTypeObject* type, const char* name, long value);

bool X_PyObject_CheckAttr (PyObject* attr, PyTypeObject* attrtype,
	const char* attrname, PyTypeObject* type);

bool X_PySequence_CheckItems (PyObject* seq, PyTypeObject* type);

PyObject* X_PyString_FromCxxString (const std::string& source);

PyObject* X_PyUnicode_FromUstring (const Glib::ustring& source);
Glib::ustring X_PyUnicode_AsUstring (PyObject* self);

//******************************************************************************

template<typename cxxtype_>
class CxxWrapperBase
{
public:

	typedef cxxtype_ cxxtype;

	struct pytype
	{
		PyObject_HEAD
		cxxtype *cxxobj;
		bool owned;
	};

	static cxxtype&
	unwrap (PyObject* self)
	{
		return *reinterpret_cast<pytype*> (self)->cxxobj;
	}

protected:

	static PyObject*
	wrap_ref (PyTypeObject* type, const cxxtype& source)
	{
		pytype *self = reinterpret_cast<pytype*> (type->tp_alloc (type, 0));
		if (self != NULL)
		{
			self->cxxobj = const_cast<cxxtype*> (&source);
			self->owned = false;
		}
		return X_PYOBJECT (self);
	}

	static void
	dealloc (pytype* self)
	{
		if (self->owned)
		{
			delete self->cxxobj;
			self->cxxobj = NULL;
		}
		self->ob_type->tp_free (X_PYOBJECT (self));
	}
};

template<typename cxxtype_>
class CxxWrapper
:	public CxxWrapperBase<cxxtype_>
{
public:

	typedef typename CxxWrapperBase<cxxtype_>::cxxtype cxxtype;
	typedef typename CxxWrapperBase<cxxtype_>::pytype pytype;

protected:

	static PyObject*
	create (PyTypeObject* type, PyObject*, PyObject*)
	{
		pytype *self = reinterpret_cast<pytype*> (type->tp_alloc (type, 0));
		if (self != NULL)
		{
			self->cxxobj = new cxxtype ();
			self->owned = true;
		}
		return X_PYOBJECT (self);
	}

	static PyObject*
	wrap_copy (PyTypeObject* type, const cxxtype& source)
	{
		pytype *self = reinterpret_cast<pytype*> (type->tp_alloc (type, 0));
		if (self != NULL)
		{
			self->cxxobj = new cxxtype (source);
			self->owned = true;
		}
		return X_PYOBJECT (self);
	}
};

#endif // PYELEMENTAL__MISC_HH
